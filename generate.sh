#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: ./generate.sh <server-ip-addr> <port>"
    exit
fi

IP=$1
PORT=$2

RTT_SIZES="1 100 200 400 800 1000"
THPUT_SIZES="1000 2000 4000 8000 16000 32000"
PROBES=20

RTT_LOG="rtt-8.log"
THPUT_LOG="thput-8.log"

if [ -f ${RTT_LOG} ];
then
    echo "Purging old ${RTT_LOG}"
    rm ${RTT_LOG}
fi

if [ -f ${THPUT_LOG} ];
then
    echo "Purging old ${THPUT_LOG}"
    rm ${THPUT_LOG}
fi

echo "Computing rtts (sending ${PROBES} probes per size)..."
for SIZE in ${RTT_SIZES}
do
    echo -n "Size: ${SIZE}... "
    ./client ${IP} ${PORT} rtt ${PROBES} ${SIZE} 800 > /dev/null
    echo "done"
done

echo "Computing throughputs (sending ${PROBES} probes per size)..."
for SIZE in ${THPUT_SIZES}
do
    echo -n "Size: ${SIZE}... "
    ./client ${IP} ${PORT} thput ${PROBES} ${SIZE} 800 > /dev/null
    echo "done"
done