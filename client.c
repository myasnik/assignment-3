#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/time.h>

#define MAX_BUF_SIZE 40000      // Maximum messages size
#define MAX_PAYLOAD_SIZE 32000  // Maximum payload size
#define MAX_PROBE_NUMBER 20     // Maximum number of probes per benchmark
#define MAX_RECV_BYTES 1400     // Maximum number of bytes to receive at a time

#define OK_CODE 200             // OK server response code

// Logs paths
#define RTT_LOG_FILE "rtt-8.log"
#define THPUT_LOG_FILE "thput-8.log"

// Receive all packets while a '\n' is found
int recv_all(int sfd, char *buf);
// Check server response
int check_server_response(char *response, ssize_t bytes);
// Generate payload for probes
void generate_payload(char *buf, int size);
// Prepare probe message
void prepare_probe(char *buf, int probe_index, char *payload);
// Prints bytes chars of a string
void print_data(char *str, ssize_t bytes);
// Log benchmark result in log files
void log_result(int probes, int lenght, double result, char *file_path);

int main(int argc, char *argv[]) {
    ssize_t bytes_recv; // Number of bytes received

    char recv_buf[MAX_BUF_SIZE + 2]; // Buffer for data to be received
    char send_buf[MAX_BUF_SIZE + 2]; // Buffer for data to be sent
    int n_probe = 0; // Number of probes

    char payload[MAX_PAYLOAD_SIZE + 2]; // "Data" section of the message

    double total_time = 0;              // Total time spent for network trasfer of probes
    int total_bytes = 0;                // Total message size in bytes sent with probes
    struct timeval  tv1, tv2; // Timer vars

    // Check if the number of passed parameters is correct, else print
    // correct form and exit
    if (argc != 7) {
        printf("\nErrore numero errato di parametri\n");
		printf("\n%s <server IP (dotted notation)> <server port> <measure type> <n probes> <msg size> <server delay>\n", argv[0]);
		exit(1);
    }

	char *address = argv[1];
	char *port = argv[2];
	char *mode = argv[3];
	char *str_n_probe = argv[4];
	char *str_payload_len = argv[5];
	char *str_delay = argv[6];
    
    // Socket and server structure initialization
    int sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sfd < 0){
        perror("Socket creation error");
		exit(EXIT_FAILURE);
    }
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(atoi(port));
    server_addr.sin_addr.s_addr = inet_addr(address);

    printf("Connecting...\n");

    // Connection phase
    int cr = connect(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (cr < 0){
        close(sfd);
        perror("Connection error"); 
        exit(EXIT_FAILURE);
    }

    // Preparing hello message
	send_buf[0] = '\0';
	sprintf(send_buf, "h %s %s %s %s\n", mode, str_n_probe, str_payload_len, str_delay);

    // Sending and printing hello message to server
    printf("Hello message sent to server: %s", send_buf);
    send(sfd, send_buf, strlen(send_buf), 0);

    // Waiting for response and printing the result
    bytes_recv = recv_all(sfd, recv_buf);
    printf("Hello message server response: ");
    print_data(recv_buf, bytes_recv);
    if (check_server_response(recv_buf, bytes_recv)) {
        close(sfd);
        perror("Bad hello message response");
        exit(EXIT_FAILURE);
    }

    // Generating the payload using the passed parameters
    int payload_len = atoi(str_payload_len);
    generate_payload(payload, payload_len);
	printf("Payload: %s\n", payload);
    
    // Parsing number of probes to send
	n_probe = atoi(str_n_probe);
    
    // Sending probes
    for(int i = 1; i <= n_probe; i++) {
        // Crafting probe message
		prepare_probe(send_buf, i, payload);

        // Printing probe message
        printf("Probe: %s", send_buf);

        // Starting timer and sending the probe
        gettimeofday(&tv1, NULL);
        send(sfd, send_buf, strlen(send_buf), 0);

        // Waiting for the echoed back probe and stopping timer
        bytes_recv = recv_all(sfd, recv_buf);
        gettimeofday(&tv2, NULL);

        // Printing echoed back probe
        printf("Received from server: ");
        print_data(recv_buf, bytes_recv);

        // Check response is same as sent data
        if (strncmp(send_buf, recv_buf, bytes_recv)) {
            close(sfd);
            perror("Bad probe response");
            exit(EXIT_FAILURE);
        }

        // Calculating probe time
        double t = (tv2.tv_sec - tv1.tv_sec) + ((double) (tv2.tv_usec - tv1.tv_usec)) / 1e6;

        // Printing single probe RTT
        printf ("Computed RTT for probe %d : %f seconds\n", i, t);

        // Updating total_time
        total_time = total_time + t;
        total_bytes += bytes_recv;
    }

    // Send bye message
    printf("Sending bye message...\n");
    send(sfd, "b\n", 2, 0);

    // Receive bye message
    bytes_recv = recv(sfd, recv_buf, sizeof(recv_buf), 0);
    printf("Server response: ");
    print_data(recv_buf, bytes_recv);

    // Printing and logging total rtt or thput
    if (strcmp(mode, "rtt") == 0){
        // total time / number of probes [s]
        double computed_rtt = total_time / n_probe;
        // convert to ms
        computed_rtt *= 1000;
        printf("Total Computed RTT (milliseconds): %f\n", computed_rtt);
        log_result(n_probe, payload_len, computed_rtt, RTT_LOG_FILE);
    } else if(strcmp(mode, "thput") == 0){
        // total bytes sent / total time [B/s]
        double computed_thput = total_bytes / total_time;
        // convert to kb/s
        computed_thput = computed_thput * 8 / 1000;
        printf("Computed Throughput: %f B/s\n", computed_thput);
        log_result(n_probe, payload_len, computed_thput, THPUT_LOG_FILE);
    }

    return 0;
}


int recv_all(int sfd, char *buf) {
    int n = 1, total = 0, found = 0;

    // Keep reading up to a '\n'
    while (!found) {
        n = recv(sfd, &buf[total], MAX_BUF_SIZE - total - 1, 0);
        if (n == -1) {
            perror("Error while receiving data in recv");
            exit(EXIT_FAILURE);
        }
        total += n;
        buf[total] = '\0';
        found = (strchr(buf, '\n') != 0);
    }

    return total;
}

int check_server_response(char *response, ssize_t bytes) {
    int code;
    int params = sscanf(response, "%d", &code);
    return (params != 1 || code != OK_CODE);
}

void generate_payload(char *buf, int size) {
    buf[0] = '\0';
    for (int i = 0; i < size; i++) {
        strcat(buf, "a");
    }
}

void prepare_probe(char *buf, int probe_index, char *payload) {
    buf[0] = '\0';
    sprintf(buf, "m %d %s\n", probe_index, payload);
}

void print_data(char *str, ssize_t bytes) {
    for(int i = 0; i < bytes; i++){
        printf("%c", str[i]);
    }
}

void log_result(int probes, int lenght, double result, char *file_path) {
    FILE* log_file = fopen(file_path, "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
    } else {
        fprintf(log_file, "%d %d %lf\n", probes, lenght, result);
        fclose(log_file);
    }
}