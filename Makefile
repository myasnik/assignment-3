C_FLAGS = -Wpedantic -Wall

all : server client

client : client.o
	gcc -o client client.o

client.o : client.c
	gcc -c ${C_FLAGS} -o client.o client.c

server : server.o
	gcc -o server server.o

server.o : server.c
	gcc -c ${C_FLAGS} -o server.o server.c

.PHONY : clean

clean :
	-rm server.o server client.o client
