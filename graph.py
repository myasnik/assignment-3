import os
import numpy as np
import matplotlib.pyplot as plt

pnum = []
sizes = []
rtts = []
with open("rtt.log", "r") as f:
    for line in f:
        data = line.split(" ")
        pnum.append(int(data[0]))
        sizes.append(int(data[1]))
        rtts.append(float(data[2]))

plt.plot(sizes, rtts, "bo-")

# pnum = []
# sizes = []
# rtts = []
# with open("rtt-1.log", "r") as f:
#     for line in f:
#         data = line.split(" ")
#         pnum.append(int(data[0]))
#         sizes.append(int(data[1]))
#         rtts.append(float(data[2]))

# plt.plot(sizes, rtts, "ro-")

# pnum = []
# sizes = []
# rtts = []
# with open("rtt-2.log", "r") as f:
#     for line in f:
#         data = line.split(" ")
#         pnum.append(int(data[0]))
#         sizes.append(int(data[1]))
#         rtts.append(float(data[2]))

# plt.plot(sizes, rtts, "go-")

# pnum = []
# sizes = []
# rtts = []
# with open("rtt-4.log", "r") as f:
#     for line in f:
#         data = line.split(" ")
#         pnum.append(int(data[0]))
#         sizes.append(int(data[1]))
#         rtts.append(float(data[2]))

# plt.plot(sizes, rtts, "yo-")

# pnum = []
# sizes = []
# rtts = []
# with open("rtt-8.log", "r") as f:
#     for line in f:
#         data = line.split(" ")
#         pnum.append(int(data[0]))
#         sizes.append(int(data[1]))
#         rtts.append(float(data[2]))

# plt.plot(sizes, rtts, "co-")



plt.title("Average rtt over payload size")
plt.xlabel("Payload size (bytes)")
plt.ylabel("Avg RTT (ms)")
# plt.legend(["no delay", "100 ms", "200 ms", "400 ms", "800 ms"])
plt.show()

pnum = []
sizes = []
thputs = []
with open("thput.log", "r") as f:
    for line in f:
        data = line.split(" ")
        pnum.append(int(data[0]))
        sizes.append(int(data[1]) / 1000)
        thputs.append(float(data[2]))

plt.semilogx(sizes, thputs, "bo-")

# pnum = []
# sizes = []
# thputs = []
# with open("thput-1.log", "r") as f:
#     for line in f:
#         data = line.split(" ")
#         pnum.append(int(data[0]))
#         sizes.append(int(data[1]) / 1000)
#         thputs.append(float(data[2]))

# plt.semilogx(sizes, thputs, "ro-")

# pnum = []
# sizes = []
# thputs = []
# with open("thput-2.log", "r") as f:
#     for line in f:
#         data = line.split(" ")
#         pnum.append(int(data[0]))
#         sizes.append(int(data[1]) / 1000)
#         thputs.append(float(data[2]))

# plt.semilogx(sizes, thputs, "go-")

# pnum = []
# sizes = []
# thputs = []
# with open("thput-4.log", "r") as f:
#     for line in f:
#         data = line.split(" ")
#         pnum.append(int(data[0]))
#         sizes.append(int(data[1]) / 1000)
#         thputs.append(float(data[2]))

# plt.semilogx(sizes, thputs, "yo-")

# pnum = []
# sizes = []
# thputs = []
# with open("thput-8.log", "r") as f:
#     for line in f:
#         data = line.split(" ")
#         pnum.append(int(data[0]))
#         sizes.append(int(data[1]) / 1000)
#         thputs.append(float(data[2]))

# plt.semilogx(sizes, thputs, "co-")


plt.title("Average throughput over message size")
plt.xlabel("Payload size (kB)")
plt.ylabel("Avg Thput (kb/s)")
# plt.legend(["no delay", "100 ms", "200 ms", "400 ms", "800 ms"])
plt.show()