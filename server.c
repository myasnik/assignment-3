#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

#define MAX_BUF_SIZE 40000      // Maximum messages size
#define MAX_PAYLOAD_SIZE 32000  // Maximum payload size
#define MAX_PROBE_NUMBER 20     // MAximum number of probes per benchmark
#define BACK_LOG 5              // Maximum queued requests

// Measurement types
#define RTT_TYPE "rtt"
#define THPUT_TYPE "thput"

typedef struct sockaddr_in ip_addr;

// Receive all packets while a '\n' is found
int recv_all(int sfd, char *buf);
// Check hello message and save parameters
int check_hello_message_and_save(char *buf, ssize_t buf_size, int *n_probes, int *msg_size, unsigned int *probe_delay);
// Check probe message
int check_probe_message(char *buf, ssize_t buf_size, int seq_num, int msg_size);
// Check bye message
int check_bye_message(char *buf, ssize_t but_size);
// Prints numBytes of a string str
void print_data(char *str, ssize_t numBytes);

// Response messages
const char *err_404_hello = "404 ERROR – Invalid Hello message\n";
const char *err_404_measurement = "404 ERROR – Invalid Measurement message\n";
const char *ok_200_hello = "200 OK - Ready\n";
const char *ok_200_closing = "200 OK – Closing\n";

int main(int argc, char *argv[]){
    // Check parameters number
    if (argc != 2) {
        printf("\nErrore numero errato di parametri\n");
        printf("\n%s <server port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // Welcome socket and server structure initialization
    int wlc_sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (wlc_sfd < 0){
        perror("Welcome socket creation error");
        exit(EXIT_FAILURE);
    }
    ip_addr server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(atoi(argv[1]));
    server_addr.sin_addr.s_addr = INADDR_ANY;
    int br = bind(wlc_sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (br < 0){
        perror("Welcome socket bind error");
        exit(EXIT_FAILURE);
    }

    // Listening for incoming requests
    int lr = listen(wlc_sfd, BACK_LOG);
    if (lr < 0){
        close(wlc_sfd);
        perror("Welcome socket listen error");
        exit(EXIT_FAILURE);
    }

    printf("Server ready for incoming connections...\n");

    // Main loop
    while(1) {
        ip_addr client_addr;
        socklen_t client_addr_size = sizeof(client_addr);

        // Wait for incoming requests and accept
        int comm_sfd = accept(wlc_sfd, (struct sockaddr *) &client_addr, &client_addr_size);
        if (comm_sfd < 0) {
            perror("Communication socket creation error");
            exit(EXIT_FAILURE);
        }

        // Printable client ip address
        char client_addr_string[INET_ADDRSTRLEN];  // String containing client address

        // Printing client infos
        inet_ntop(AF_INET, &client_addr.sin_addr, client_addr_string, INET_ADDRSTRLEN);
        printf("Connection received from IP %s and port %d\n", client_addr_string, ntohs(client_addr.sin_port));

        // Fork the process to handle multiple connections
        if (fork() == 0) {
            // Child process

            // Close welcome socket
            close(wlc_sfd);

            int n_probes;                                       // Number of probes
            int msg_size;                                       // Message size
            unsigned int probe_delay = 0;                       // Probe delay (milliseconds)

            // Data buffer and byte counts
            char recv_buf[MAX_BUF_SIZE];
            ssize_t bytes_recv, bytes_sent;

            // Receiving hello message
            bytes_recv = recv_all(comm_sfd, recv_buf);
            if (bytes_recv < 0){
                close(comm_sfd);
                perror("Hello message receive error");
                exit(EXIT_FAILURE);
            }
            
            // Check hello message and save benchmark info
            if (check_hello_message_and_save(recv_buf, bytes_recv, &n_probes, &msg_size, &probe_delay) != 0) {
                printf("%s:%d -> %s", client_addr_string, ntohs(client_addr.sin_port), err_404_hello);
                bytes_sent = send(comm_sfd, err_404_hello, strlen(err_404_hello), 0);
                close(comm_sfd);
                printf("Hello message error, invalid hello message\n");
                exit(EXIT_FAILURE);
            }

            printf("%s:%d -> Received hello message - Bytes received: %ld - Content:\n", client_addr_string, ntohs(client_addr.sin_port), bytes_recv);
            print_data(recv_buf, bytes_recv);

            printf("%ld\n", strlen(recv_buf));

            // Print OK status and send OK
            printf("%s:%d -> %s", client_addr_string, ntohs(client_addr.sin_port), ok_200_hello);
            bytes_sent = send(comm_sfd, ok_200_hello, strlen(ok_200_hello), 0);

            // Probe loop (for n_probes)
            for(int probe_seq = 1; probe_seq <= n_probes; probe_seq++) {
                // Receive probe
                bytes_recv = recv_all(comm_sfd, recv_buf);
                if (bytes_recv < 0){
                    close(comm_sfd);
                    perror("Probe receive error");
                    exit(EXIT_FAILURE);
                }

                // Check probe message
                if (check_probe_message(recv_buf, bytes_recv, probe_seq, msg_size) != 0) {
                    printf("%s:%d -> %s", client_addr_string, ntohs(client_addr.sin_port), err_404_measurement);
                    bytes_sent = send(comm_sfd, err_404_measurement, strlen(err_404_measurement), 0);
                    printf("Probe message error, invalid probe message\n");
                    exit(EXIT_FAILURE);
                }

                // Print probe, simulate delay, and send back the probe
                printf("%s:%d -> Received probe number %d - Bytes received: %ld - Content:\n", client_addr_string, ntohs(client_addr.sin_port), probe_seq, bytes_recv);
                print_data(recv_buf, bytes_recv);
                
                usleep(probe_delay * 1000);

                bytes_sent = send(comm_sfd, recv_buf, bytes_recv, 0);
                if(bytes_sent != bytes_recv){
                    close(comm_sfd);
                    perror("Probe message error, can't send back");
                    exit(EXIT_FAILURE);
                }

                printf("%s:%d -> Sent back probe number %d\n", client_addr_string, ntohs(client_addr.sin_port), probe_seq);
            }

            // Receive bye message
            bytes_recv = recv_all(comm_sfd, recv_buf);
            if (bytes_recv < 0){
                perror("Bye message error, can't receive bye message");
                exit(EXIT_FAILURE);
            }

            // Check that is not a probe
            if (recv_buf[0] == 'm') {
                printf("%s:%d -> %s", client_addr_string, ntohs(client_addr.sin_port), err_404_measurement);
                bytes_sent = send(comm_sfd, err_404_measurement, strlen(err_404_measurement), 0);
                printf("Probe message error, too many probes sent\n");
                exit(EXIT_FAILURE);
            }

            // Check bye message and reply OK
            if (check_bye_message(recv_buf, bytes_recv) == 0) {
                printf("%s:%d -> Received by message - Bytes received: %ld - Content:\n", client_addr_string, ntohs(client_addr.sin_port), bytes_recv);
                print_data(recv_buf, bytes_recv);
                bytes_sent = send(comm_sfd, ok_200_closing, strlen(ok_200_closing), 0);
                printf("%s:%d -> %s", client_addr_string, ntohs(client_addr.sin_port), ok_200_closing);
            }
            else {
                close(comm_sfd);
                printf("Bye message error, wrong bye message\n");
                exit(EXIT_FAILURE);
            }

            close(comm_sfd);
            exit(EXIT_SUCCESS);
        }
        else {
            // Father process
            close(comm_sfd);
        }
    }

    close(wlc_sfd);
    return 0;
}

int recv_all(int sfd, char *buf) {
    int n = 1, total = 0, found = 0;

    // Keep reading up to a '\n'
    while (!found) {
        n = recv(sfd, &buf[total], MAX_BUF_SIZE - total - 1, 0);
        if (n == -1) {
            perror("Error while receiving data in recv");
            exit(EXIT_FAILURE);
        }
        total += n;
        buf[total] = '\0';
        found = (strchr(buf, '\n') != 0);
    }

    return total;
}

int check_hello_message_and_save(char *buf, ssize_t buf_size, int *n_probes, int *msg_size, unsigned int *probe_delay) {
    char measure_type[8];       // Store measurement type

    int read_chars;             // Total number of chars read by sscanf until \n
    // Parse hello message and check all parameters are there
    int params = sscanf(buf, "h %s %d %d %u%n\n", measure_type, n_probes, msg_size, probe_delay, &read_chars);
    if (params != 4 || read_chars != buf_size - 1) {
        return 1;
    }

    // Check for wrong measurement type
    if (strcmp(measure_type, RTT_TYPE) != 0 && strcmp(measure_type, THPUT_TYPE) != 0) {
        return 1;
    }

    // Check probe number
    if (*n_probes > MAX_PROBE_NUMBER) {
        return 1;
    }

    // Check payload size
    if (*msg_size > MAX_PAYLOAD_SIZE) {
        return 1;
    }

    return 0;
}

int check_probe_message(char *buf, ssize_t buf_size, int seq_num, int msg_size) {
    int recv_seq_num;
    char payload[MAX_PAYLOAD_SIZE + 2];

    int read_chars;            // Total number of chars read by sscanf until \n
    // Parse measurement probe message and check all parameteres are there
    int params = sscanf(buf, "m %d %s%n\n", &recv_seq_num, payload, &read_chars);
    if (params != 2 || read_chars != buf_size - 1) {
        return 1;
    }

    // Check sequence number
    if (recv_seq_num != seq_num) {
        return 1;
    }

    // Check payload size
    if (strlen(payload) != msg_size) {
        return 1;
    }

    return 0;
}

int check_bye_message(char *buf, ssize_t buf_size) {
    return strncmp(buf, "b\n", buf_size);
}

void print_data(char *str, ssize_t numBytes) {
    for(int i = 0; i < numBytes; i++){
        printf("%c", str[i]);
    }
}

